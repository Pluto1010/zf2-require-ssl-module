<?php
/**
 * Service Configuration
 */
return array(
	'factories' => array(
		'NetglueSSL\Service\Options' => 'NetglueSSL\Service\OptionsFactory',
		'NetglueSSL\Service\UriResolver' => 'NetglueSSL\Service\UriResolverFactory',
	),
);
